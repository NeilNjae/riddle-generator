My first is in shoat but not in oath
My second is in orate but not in ratter
My third is in preposition but not in osteoporosis
My fourth is in astern but not in taster
My fifth is in conscientiousness but not in suction
My sixth is in immorality but not in immorally

My first is in riddle, but not in little.
My second is in think, but not in brink.
My third is in thyme, but not in time.
My fourth is in mother, but not in brother.
My last is in time, but not in climb.

My first is in spell, but not book.
My second is in fright and also shook.
My third is in cauldron, but never in pot.
My fourth is in net and also in knot.
My fifth is in bat, but never in vampire.
My sixth is in coal, but not found in fire.
My seventh is in moon, but not in night.