---
jupyter:
  jupytext:
    formats: ipynb,md,py:percent
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Generate a dictionary of related words

```python
import unicodedata
import re
from dataclasses import dataclass
from typing import Dict, Tuple, List, Set
from enum import Enum, auto
import functools
import random
import multiprocessing
import gzip
# import csv
```

```python
stop_words = set('my is in within lies and also always you will find the found but'.split())
negative_words = set('not never neither nor'.split())
```

```python
ordinals : Dict[str, int] =  { 'last': -1
            , 'first': 1
            , 'second': 2
            , 'third': 3
            , 'fourth': 4
            , 'fifth': 5
            , 'sixth': 6
            , 'seventh': 7
            , 'eighth': 8
            , 'ninth': 9
            , 'tenth': 10
            , 'eleventh': 11
            , 'twelfth': 12
            }

reverse_ordinals : Dict[int, str] = {n: w for w, n in ordinals.items()}
```

```python
dictionary : List[str] = [unicodedata.normalize('NFKD', w.strip()).\
                 encode('ascii', 'ignore').\
                 decode('utf-8')
              for w in open('/usr/share/dict/british-english').readlines()
              if w.strip().islower()
              if w.strip().isalpha()
              if len(w.strip()) >= 5
              if len(w.strip()) <= 12
              if w not in stop_words
              if w not in negative_words
              if w not in ordinals
             ]
```

Some types that will be used throughout the library

```python
@functools.lru_cache
def edit_distance(s: str, t: str) -> int:
  if s == "":
    return len(t)
  if t == "":
    return len(s)
  if s[0] == t[0]:
    cost = 0
  else:
    cost = 1
       
  res = min(
    [ edit_distance(s[1:], t) + 1
    , edit_distance(s, t[1:]) + 1
    , edit_distance(s[1:], t[1:]) + cost
    ])

  return res
```

```python
# def find_neighbours(word: str, limit: int = 4) -> Tuple[str, List[str]]:
def find_neighbours(word, limit=4):
  sword = set(word)
  others = []
  for other in dictionary:
    if other != word:
      soth = set(other)
      if (not sword <= soth and 
          not soth <= sword and 
          edit_distance(word, other) <= limit):
        others.append(other)
  return word, others
```

```python
with multiprocessing.Pool() as pool:
  # word_other_pairs = pool.imap_unordered(find_neighbours, dictionary, chunksize=5000)
  word_other_pairs = pool.map(find_neighbours, dictionary)
```

```python
with gzip.open('dictionary_neighbours.txt.gz', 'wt') as f:
  for word, related in word_other_pairs:
    f.write(f'{word},{",".join(related)}\n')
```

```python

```
