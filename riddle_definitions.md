---
jupyter:
  jupytext:
    formats: ipynb,md,py:percent
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import unicodedata
import re
from dataclasses import dataclass
from typing import Dict, Tuple, List, Set
from enum import Enum, auto
import functools
import random
```

```python
stop_words = set('my is in within lies and also always you will find the found but'.split())
negative_words = set('not never neither nor'.split())
```

```python
ordinals : Dict[str, int] =  { 'last': -1
            , 'first': 1
            , 'second': 2
            , 'third': 3
            , 'fourth': 4
            , 'fifth': 5
            , 'sixth': 6
            , 'seventh': 7
            , 'eighth': 8
            , 'ninth': 9
            , 'tenth': 10
            , 'eleventh': 11
            , 'twelfth': 12
            }

reverse_ordinals : Dict[int, str] = {n: w for w, n in ordinals.items()}

def from_ordinal(word: str) -> int:
  return ordinals[word]

def to_ordinal(number: int) -> str:
  return reverse_ordinals[number]
```

These are the words that can be the solution to a riddle, and used as the clue for a riddle.

```python
dictionary : List[str] = [unicodedata.normalize('NFKD', w.strip()).\
                 encode('ascii', 'ignore').\
                 decode('utf-8')
              for w in open('/usr/share/dict/british-english').readlines()
              if w.strip().islower()
              if w.strip().isalpha()
              if len(w.strip()) >= 5
              if len(w.strip()) <= 12
              if w not in stop_words
              if w not in negative_words
              if w not in ordinals
             ]
```

Some types that will be used throughout the library

```python
class RiddleValence(Enum):
  """Does this part of the riddle include or exclude letters?"""
  Include = auto()
  Exclude = auto()

@dataclass
class RiddleClue:
  """A half line of a riddle, like 'is in dreams' or 'not in octet'"""
  valence : RiddleValence
  word : str
  
@dataclass
class RiddleElement:
  """A representation of the constraints that come from a whole line of 
  a riddle"""
  valence : RiddleValence
  letters : Set[str]

# A riddle that's been read and parsed. 
# Note that the numbering is one-based, as per the numbers in the riddle text
Riddle = Dict[int, Tuple[RiddleClue, RiddleClue]]

# A riddle that's been processed ready for solving
# Note that the numbering is one-based, as per the numbers in the riddle text
RiddleElems =  Dict[int, RiddleElement]
```

```python
@functools.lru_cache
def edit_distance(s: str, t: str) -> int:
  if s == "":
    return len(t)
  if t == "":
    return len(s)
  if s[0] == t[0]:
    cost = 0
  else:
    cost = 1
       
  res = min(
    [ edit_distance(s[1:], t) + 1
    , edit_distance(s, t[1:]) + 1
    , edit_distance(s[1:], t[1:]) + cost
    ])

  return res
```

```python
def collapse_riddle_clues(elems : Riddle) -> RiddleElems:
  """Combine the two parts of a riddle line into one element for solving.
  This takes account of the valence of the two parts."""
  def combine_clues(a: RiddleClue, b: RiddleClue) -> RiddleElement:
    if a.valence == b.valence:
      if a.valence == RiddleValence.Include:
        return RiddleElement(letters = set(a.word) & set(b.word), 
                             valence = RiddleValence.Include)
      else:
        return RiddleElement(letters = set(a.word) | set(b.word), 
                             valence = RiddleValence.Exclude)
    else:
      if a.valence == RiddleValence.Include:
        p, q = a, b
      else:
        p, q = b, a
      return RiddleElement(letters = set(p.word) - set(q.word), 
                           valence = RiddleValence.Include)
  
  return {i: combine_clues(a, b) for i, (a, b) in elems.items()}
```

```python
def matches_element(pos: int, elem: RiddleElement, word: str) -> bool:
  """Does this element match this position of the the word?
  Note that positions are one-based, as in the numbering system of the 
  puzzle."""
  if len(word) < pos:
    return False
  if elem.valence == RiddleValence.Include:
    return word[pos-1] in elem.letters
  else:
    return word[pos-1] not in elem.letters
```

```python
def matches_all_elements(riddle: RiddleElems, word: str) -> bool:
  """Do all the elements of a riddle match the appropriate parts of a word?"""
  if -1 in riddle:
    last_elem = riddle[-1]
    new_riddle = {p: e for p, e in riddle.items() if p != -1}
    new_riddle[len(word)] = last_elem
  else:
    new_riddle = riddle
  return all(matches_element(i, elem, word) for i, elem in new_riddle.items())
```

```python
def solve_riddle(riddle: RiddleElems) -> List[str]:
  """Find all words that match this riddle"""
  return [w for w in dictionary 
          if len(w) == len(riddle)
          if matches_all_elements(riddle, w)]    
```

```python

```
