# Riddle solving and generation

> My first is in crankshaft but not in camshaft<br />
> My second is in sincere but not in cancer<br />
> My third is in hardtop but not in carton<br />
> My fourth is in deliberation but not in celebration<br />
> My fifth is in flail but not in await<br />
> My sixth is in both beetling and nestling<br />
> What am I?
>
> Answer: riddle

This repository is code written to solve and generate riddles of the form above. These programs are fully discussed on my blog in a three-part series. You should read those articles to understand how this code works and why I organised it like I did.

1. [Solving riddles](https://work.njae.me.uk/2023/08/15/solving-riddles/)
2. [Generating riddles](https://work.njae.me.uk/2023/08/30/generating-riddles/)
3. [Optimising riddle generation](https://work.njae.me.uk/2023/09/01/optimising-riddle-generation/)

The files here are:

* [Riddle solver](riddle_solver.md): solves riddles given in text
* [Riddle creator](riddle_creator.md): generates new riddles, from given or random words
* [A faster riddle creator](riddle_creator_filtered.md)
* [A much faster riddle creator](riddle_creator_lazy.md)
* [Riddle definitions](riddle_definitions.md): some definitions used across the code base

The last two creation programs use a pre-processed word list; the [pre-processor program](riddle_dict_builder.md) is also here. There is a [file that analyses the performance of the different riddle creation programs](creation_analysis.md).

You can find the code on [my Git server](https://git.njae.me.uk/?p=riddle-generator.git;a=summary) or on [Gitlab](https://gitlab.com/NeilNjae/riddle-generator).

## Licence

The code here is published under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/) licence.